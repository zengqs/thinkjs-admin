const Schema =think.Mongoose.Schema;

module.exports  = class extends think.Mongoose {
    get schema() {
        const schema = new Schema({
            title: {
                type: String,
                required: true,
                index:true,
            },
            pinyin: String,
            latitude: Number,//经度
            longitude: Number,//纬度
            address: String,
            cover: {
                type: Schema.Types.ObjectId,
                ref: 'Picture',//关联File表的_id
            },
            tags: [String],
            status: {type: Number, default: 0},
        }, {timestamps: {}, minimize: false});

        //手动初始化ref表
        think.mongoose('Picture',{},'admin');

        schema.set('toJSON', { virtuals: true });
        schema.set('toObject', { virtuals: true });

        return schema;
    }
};