module.exports = {
    'name': 'datacenter',    //模块名
    'alias': '数据中心', //别名
    'version': '1.0.0',//版本号
    'showNav': 1,//是否显示在导航栏内？  1是，0否
    'summary': '平台数据中心',
    'developer': '青伢子',
    'website': 'http://www.zengqs.com',
    'entry': '/datacenter/index/index', //前台入口
    'adminEntry': '/datacenter/mapmark/index',
    'icon': 'bug', //支持Font Awesome字体图标，不需要fa-前缀
    'canUninstall': true
};