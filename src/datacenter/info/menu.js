module.exports = async function () {
    /**
     * 支持2级菜单
     * 基本原则：
     * 按照模块分组菜单
     */
    //创建模块的主菜单id
    const id = new think.Mongoose.mongoose.Types.ObjectId();
    const MenuModel = think.mongoose('Menu');

    //删除可能已经存在的不一致的数据
    await MenuModel.remove({module: 'datacenter'}).exec();

    // MenuModel.create({
    //     _id: id,
    //     parent: null,
    //     title: null,//菜单分组，必须设置为null
    //     tip: '',
    //     rank: 0,
    //     hide: true,
    //     url: null,
    //     group: '数据中心',
    //     module: 'datacenter',
    //     icon: null
    // });

    MenuModel.create({
        _id: new think.Mongoose.mongoose.Types.ObjectId(),
      //  parent: id,
        title: '地图数据管理',
        tip: '地图标记数据管理',
        rank: 0,
        hide: false,
        url: '/datacenter/mapmark/index',
        group: '数据中心',
        module: 'datacenter',
        icon: null
    });
};