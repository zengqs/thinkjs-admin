// const {slugify} =require('transliteration');
const Base = require('./base.js');

module.exports = class extends Base {

    async _initialize() {
        await super._initialize();
    }

    async indexAction() {
        this.assign({
            env:think.env,
            version:think.version,
        });
        await this.display('admin/index/index');
    }
}