const Base = require('./base.js');

module.exports = class extends Base {
    // async __before(){
    //     // super.__before();
    //
    //     // const allowed = await super.checkAccessPermission('manage_account');
    // }

    async indexAction() {
        this.assign({
                status: [{key: '0', title: '关闭'}, {key: '1', title: '开启'}]
            }
        );
        await this.display('admin/administrator/index');
    }

    async tableAction() {

        const p = this.query('page') || 1;
        const id = this.post('id');
        const status = this.post('status');
        const keywords = this.post('keywords');

        //构造查询条件
        let queryParams = {};

        //正则表达式查询
        if (keywords !== "") {
            queryParams = {
                $or: [
                    {username: {$regex: keywords, $options: "$i"}},
                    {email: {$regex: keywords, $options: "$i"}},
                    {mobile: {$regex: keywords, $options: "$i"}}
                ]
            }
        }

        //等值查询
        if (status !== "") {
            queryParams = Object.assign(queryParams, {status: status});
        }

        //id属性要单独查找,不支持正则表达式
        if (id !== "") {
            try {
                queryParams = {
                    _id: think.Mongoose.mongoose.Types.ObjectId(id)
                }
            } catch (err) {
                this.error(1001, '没找到符合条件的记录');
                return;
            }
        }

        const Model = this.mongoose('Administrator', {}, 'admin');
        const pageSize = this.mongoose('Config', {}, 'admin').C('LIST_ROWS');
        const pageEntity = await Model.findPage({
            page: p,
            pageSize: pageSize
        }, 'roles', queryParams, {}, {username: 'asc'});

        this.assign({
            records: pageEntity.data,
            pager: super.ajaxPage(p, pageEntity.pagination.rowCount, pageEntity.pagination.pageSize)
        });

        // console.log(this.assign());

        await this.display('admin/administrator/table');
    }

    async disableAction() {
        const id = this.post('id');
        const ids = id.split(',');
        let Model = this.mongoose('Administrator');
        for (const i of ids) {
            await Model.findByIdAndUpdate(i, {status: 0}).exec();
        }
        this.success('', '禁用用户成功');

    }

    async enableAction() {
        const id = this.post('id');
        const ids = id.split(',');
        let Model = this.mongoose('Administrator');
        for (const i of ids) {
            await Model.findByIdAndUpdate(i, {status: 1}).exec();
            //this.changeModelField('admin/User', i, 'status', 1);
        }
        this.success('', '开启用户成功');
    }

    async deleteAction() {
        const id = this.post.id || "";
        const ids = id.split(',');
        let Model = this.mongoose('Administrator');
        for (const i of ids) {
            // await Model.findByIdAndRemove(i).exec();
            await Model.findUserByIdAndRemove(i);//删除关联数据
        }
        this.success('/admin/administrator/index', '删除用户成功');
    }

    async addAction() {

        if (this.isPost) {

            const administrator = this.post();

            if (administrator.password) {
                administrator.password = md5(administrator.password);
            } else {
                delete administrator.password;
            }

            const Model = this.mongoose('Administrator');
            if (!think.isEmpty(this.post('id'))) {
                await Model.findByIdAndUpdate(this.post('id'), administrator).exec();
                this.success('/admin/administrator/index', '更新成功');
            } else {
                await Model.create(administrator).exec();
                this.success('/admin/administrator/index', '创建新项成功');
            }

        } else {

            if (this.query('id')) {
                const administrator = await this.mongoose('Administrator').findOne({_id: this.query('id')}).exec();
                this.assign({administrator: administrator});
                await this.display('admin/administrator/add');
            } else {
                await this.display('admin/administrator/add');
            }
        }
    }
};