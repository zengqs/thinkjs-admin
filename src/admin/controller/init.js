const Base = require('./base.js');

module.exports = class extends think.Controller {
    async indexAction(){
        // 初始化系统数据
        const Types = think.Mongoose.mongoose.Types;
        const Administrator = this.mongoose('Administrator');
        let Permission = this.mongoose('Permission');
        const Role = this.mongoose('Role');
        const Config = this.mongoose('Config');
        const Module = this.mongoose('Module');
        const Menu = this.mongoose('Menu');
        const Picture = this.mongoose('Picture');
        const User = this.mongoose('User');


        await Administrator.findOne(async function (err, data) {
           const self=this;
            if (!data) {

                await User.create({
                    _id: new Types.ObjectId(),
                    username: `user`,
                    password: '21232f297a57a5a743894a0e4a801fc3',
                    email: `user@test.com`,
                    mobile: 18929540378,
                    deletable: false,
                    editable: true
                });

                let id;

                await Administrator.create({
                    _id: new Types.ObjectId(),
                    username: `admin`,
                    password: '21232f297a57a5a743894a0e4a801fc3',
                    email: `admin@test.com`,
                    mobile: 18929540378,
                    deletable: false,
                    editable: true
                });

                await Administrator.create({
                    _id: new Types.ObjectId(),
                    username: `system`,
                    password: '21232f297a57a5a743894a0e4a801fc3',
                    email: `system@test.com`,
                    mobile: 18928779564,
                    deletable: false,
                    editable: true
                });

                const manageSystemPermissionId = new Types.ObjectId();
                const manageAccountPermissionId = new Types.ObjectId();

                await Permission.create(
                    {
                        _id: manageSystemPermissionId,
                        code: 'manage_system',
                        title: '管理系统',
                        description: '后台管理的基本权限',
                        module: 'admin'
                    }
                );

                await Permission.create(
                    {
                        _id: manageAccountPermissionId,
                        code: 'manage_account',
                        title: '管理系统账户信息',
                        description: '管理系统的用户信息，包含对用户的权限的分配和帐号的删除',
                        module: 'admin'
                    }
                );


                const adminRoleId = new Types.ObjectId();
                const systemRoleId = new Types.ObjectId();

                //一般的管理员必须具有管理系统的基本权限才能访问后台
                await Role.create(
                    {
                        _id: adminRoleId,
                        parent: null,
                        title: '一般管理员',
                        code: 'admin',
                        permissions: [manageSystemPermissionId]
                    }
                );


                //系统超级管理元才能管理账户信息
                await Role.create(
                    {
                        _id: systemRoleId,
                        parent: adminRoleId,//超级管理员自动拥有管理员的权限
                        code: 'system',
                        title: '系统超级管理员',
                        permissions: [manageAccountPermissionId]
                    }
                );

                //给admin账户授予超级管理员角色
                let system = await  Administrator.findOne({username: 'system'}).exec();
                await Administrator.findByIdAndUpdate(system._id, {$push: {roles: systemRoleId}}).exec();

                let admin = await Administrator.findOne({username: 'admin'}).exec();
                await Administrator.findByIdAndUpdate(admin._id, {$push: {roles: adminRoleId}}).exec();




                await Config.create({
                    key: `CONFIG_GROUP_LIST`,
                    title: '配置分组',
                    value: '0:不分组|1:基本|2:内容|3:用户|4:系统|5:邮件',
                    group: 4,
                    valueType: 'Array',
                    deletable: false,
                    editable: false
                });
                await Config.create({
                    key: `LIST_ROWS`,
                    title: '后台每页记录数',
                    value: 10,
                    valueType: 'Number',
                    group: 2,
                    deletable: false,
                    editable: true
                });






                id = new Types.ObjectId();
                await Menu.create({
                    _id: id,
                    parent: null,
                    title: null,
                    tip: '',
                    rank: 0,
                    hide: true,
                    url: null,
                    group: '系统',
                    module: 'admin',
                    icon: null
                });

                await Menu.create({
                    _id: new Types.ObjectId(),
                    parent: id,
                    title: '配置管理',
                    tip: '系统配置管理',
                    rank: 0,
                    hide: false,
                    url: '/admin/config/index',
                    group: '系统',
                    module: 'admin',
                    icon: null
                });

                await Menu.create({
                    _id: new Types.ObjectId(),
                    parent: id,
                    title: '模块管理',
                    tip: '系统模块管理',
                    rank: 1,
                    hide: false,
                    url: '/admin/module/index',
                    group: '系统',
                    module: 'admin',
                    icon: null
                });


                await Menu.create({
                    _id: new Types.ObjectId(),
                    parent: id,
                    title: '插件管理',
                    tip: '系统插件管理',
                    rank: 2,
                    hide: false,
                    url: '/admin/addon/index',
                    group: '系统',
                    module: 'admin',
                    icon: null
                });


                await Menu.create({
                    _id: new Types.ObjectId(),
                    parent: id,
                    title: '图片管理',
                    tip: '系统图片管理',
                    rank: 4,
                    hide: false,
                    url: '/admin/picture/index',
                    group: '系统',
                    module: 'admin',
                    icon: null
                });

                await Menu.create({
                    _id: new Types.ObjectId(),
                    parent: id,
                    title: '后台菜单管理',
                    tip: '后台管理系统的导航菜单维护',
                    rank: 5,
                    hide: false,
                    url: '/admin/menu/index',
                    group: '系统',
                    module: 'admin',
                    icon: null
                });


            }
        }).exec();


        const data = await Administrator.find().exec();
        console.log(data);
        this.body = '框架数据初始化!';
    }


    async initConfig(){
        let Model=this.mongoose('Config');

    }
};


