const Schema = think.Mongoose.Schema;

module.exports = class extends think.Mongoose {
    get schema() {
        const schema = new Schema({
            parent: {type: Schema.Types.ObjectId, ref: `${this.tablePrefix}Menu`, default: null},
            title: {type: String, default: null},
            tip: String,
            rank: {type: Number, default: 0},
            hide: {type: Boolean, default: false},
            url: String,
            group: String,
            module: String,
            icon: String,
        }, {timestamps: {}, minimize: false});


        schema.statics.getMenuGroups = async function () {
            const groups = await this.distinct('group').exec();
            return groups;
        };

        // schema.statics.getMenuGroup = async function (group) {
        //     const topGroup = await this.findOne({
        //         group: group,
        //         title: {$eq: null}
        //     }).exec();
        //     return topGroup;
        // };

        schema.statics.getMenuByGroup = async function (group) {
            const itemsInGroup = await this.find({
                group: group,
                title: {$ne: null},
                hide:false
            }).sort({rank: 'asc'}).exec();
            return itemsInGroup;
        };

        schema.set('toJSON', { virtuals: true });
        schema.set('toObject', { virtuals: true });

        return schema;
    }
};