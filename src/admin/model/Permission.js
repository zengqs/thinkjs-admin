const Schema = think.Mongoose.Schema;
module.exports = class extends think.Mongoose {
    get schema() {
        const schema = new Schema({
            code: {type: String, required: true}, //权限的代码，唯一
            title: {type: String, required: true},//权限的名称：代码的中文解释
            description: String,//权限的描述性文字
            module: String //权限定义所属的模块，不同模块可以定义同名（code)的权限
        }, {timestamps: {}, minimize: false});

        schema.set('toJSON', { virtuals: true });
        schema.set('toObject', { virtuals: true });

        return schema;
    }
};
