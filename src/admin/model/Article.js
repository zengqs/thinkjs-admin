// src/admin/model/administrator.js
const Schema =think.Mongoose.Schema;

module.exports  = class extends think.Mongoose {
    get schema() {
        const schema = new Schema({
            title: {
                type: String,
                required: true
            },
            pinyin: String,
            url: Boolean,
            cover: {
                type: Schema.Types.ObjectId,
                ref: `${this.tablePrefix}Picture`,//关联File表的_id
            },
            category: {
                type: Schema.Types.ObjectId,
                ref: `${this.tablePrefix}Category` //关联Category表的_id
            },
            author: String,
            editor: [], //编辑，经过编辑的人列表
            body: String,
            abstract: String,
            tags: [],
            articleType: String,
            //  `type` int(11) NOT NULL COMMENT '0:最新资讯，1:推荐阅读，2:今日头条，3:电商经验谈',
            status: {type: Number, default: 0},
            rank: {type: Number, default: 0}, //排序
            meta: {
                favors: {type: Number, default: 0},
                visited: {type: Number, default: 0}
            }
        }, {timestamps: {}, minimize: false});

        //手动初始化ref表,如果跨模块的模型要填写模块的名称
        think.mongoose('Picture',{},'admin');
        think.mongoose('Category',{},'admin');

        schema.set('toJSON', { virtuals: true });
        schema.set('toObject', { virtuals: true });

        return schema;
    }
};