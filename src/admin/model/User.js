// src/admin/model/administrator.js
const Schema =think.Mongoose.Schema;

module.exports  = class extends think.Mongoose {
    get schema() {
        const schema = new Schema({
            username: {
                type: String,
                required: true
            },
            password: {
                type: String,
                required: true
            },
            mobile: String,
            email: String,
            status: {
                type: Number,
                default: 1
            },
            remark: {
                type: String,
                default: ''
            },
            deletable: {type: Boolean, default: true},
            editable: {type: Boolean, default: true},
        }, {timestamps: {}, minimize: false});

        schema.set('toJSON', { virtuals: true });
        schema.set('toObject', { virtuals: true });

        schema.statics.findUserByIdAndRemove= async function(id){
            const UserExtraInfo= think.mongoose('UserExtraInfo');
            const userExtraInfo = await UserExtraInfo.findOne({user:id}).exec();
            if (userExtraInfo){
                userExtraInfo.remove();
            }
            await this.findByIdAndRemove(id).exec();
        };

        return schema;
    }
};