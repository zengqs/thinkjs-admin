// src/admin/model/administrator.js
const Schema =think.Mongoose.Schema;

module.exports  = class extends think.Mongoose {
    get schema() {
        const schema = new Schema({
            user: {type: Schema.Types.ObjectId, ref: 'User'},
            name: String,
            pinyin:String,
            photo: {
                type: Schema.Types.ObjectId,
                ref: 'Picture' //关联Picture表的_id
            },
            passport: {
                name: String, //姓名
                code: String,//证件号码
                authenticated: {type: Boolean, default: false},//是否已经实名认证
                snapA: {type: Schema.Types.ObjectId, ref: `${this.tablePrefix}Picture`},//身份证照片正面
                snapB: {type: Schema.Types.ObjectId, ref: `${this.tablePrefix}Picture`},//身份证照片背面
                issuingAuthority: String, //发证机关
                passportType: {type: String, default: '身份证'} //证件类型
            },
            remark: {
                type: String,
                default: ''
            },
            deletable: {type: Boolean, default: true},
            editable: {type: Boolean, default: true},
        }, {timestamps: {}, minimize: false});

        //手动初始化ref表,如果跨模块的模型要填写模块的名称
        think.mongoose('Picture',{},'admin');
        think.mongoose('User',{},'admin');

        schema.set('toJSON', { virtuals: true });
        schema.set('toObject', { virtuals: true });

        return schema;
    }
};