const Schema = think.Mongoose.Schema;
module.exports = class extends think.Mongoose {
    get schema() {
        const schema = new Schema({
            roleId: {type: Schema.Types.ObjectId, ref: `${this.tablePrefix}Role`, required: true},
            menuId: {type: Schema.Types.ObjectId, ref: `${this.tablePrefix}Menu`, required: true},
        }, {timestamps: {}, minimize: false});

        think.mongoose('Role', {}, 'admin');
        think.mongoose('Menu', {}, 'admin');



        schema.statics.authMenuForRole = async function (roleId, menuId) {
            if (!(await this.findOne({roleId: roleId, menuId: menuId}).exec())) {
                let obj = new this({
                    roleId: roleId, menuId: menuId
                });
                await obj.save();
            }
        };

        schema.statics.revokeMenuFromRole = async function (roleId, menuId) {
            await this.remove({roleId: roleId, menuId: menuId}).exec();
        };




        schema.set('toJSON', {virtuals: true});
        schema.set('toObject', {virtuals: true});

        return schema;
    }
};

