// src/admin/model/administrator.js
const Schema =think.Mongoose.Schema;

module.exports  = class extends think.Mongoose {
    get schema() {
        const schema = new Schema({
            parent: {type: Schema.Types.ObjectId, ref: `${this.tablePrefix}Comment`, default: null},
            commenter: {type: Schema.Types.ObjectId, ref: `${this.tablePrefix}User`, default: null},
            body: String,
            application: String,
            model: {type: String, default: 'Article'}, //记录评论的模型
            rowId: {type: Schema.Types.ObjectId},//记录评论的模型的ID
            module: String,
            status:{type:Number,default:1},
            ip: String
        }, {timestamps: {}, minimize: false});

        //手动初始化ref表,如果跨模块的模型要填写模块的名称
        think.mongoose('User',{},'admin');


        schema.set('toJSON', { virtuals: true });
        schema.set('toObject', { virtuals: true });

        return schema;
    }
};