// src/admin/model/administrator.js
const Schema =think.Mongoose.Schema;

module.exports  = class extends think.Mongoose {
    get schema() {
        const schema = new Schema({
            name: {type: String, required: true},
            alias: String,
            summary:String,
            version: {type: String, default: '1.0.0'},
            showNav: {type: Boolean, default: true},
            developer: {type: String, default: '青伢子'},
            website: String,
            entry: String,
            adminEntry: String,
            icon: String,
            canUninstall: {type: Boolean, default: true},
            isSetup: {type: Boolean, default: false},
            authRole: [Schema.Types.ObjectId],
            // authRole: [{type:ObjectId, ref:'Role'}]
        }, {timestamps: {}, minimize: false});

        schema.set('toJSON', { virtuals: true });
        schema.set('toObject', { virtuals: true });

        schema.statics.getModules = async function () {
            return await this.find().exec();
        };

        return schema;
    }
};