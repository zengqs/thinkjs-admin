const Schema =think.Mongoose.Schema;
module.exports = class extends think.Mongoose {
    get schema() {
        const schema = new Schema({
            parent: {type: Schema.Types.ObjectId, ref: `${this.tablePrefix}Role`},
            code: {type: String, required: true},
            title: {type: String, required: true},
            description: String,
            permissions: [{type: Schema.Types.ObjectId, ref: `${this.tablePrefix}Permission`}]
        }, {timestamps: {}, minimize: false});

        think.mongoose('Permission',{},'admin');

        schema.set('toJSON', { virtuals: true });
        schema.set('toObject', { virtuals: true });

        return schema;
    }
};

