module.exports = class extends think.Logic {
    async loginAction() {
        this.allowMethods = 'get,post'; // 允许 GET、POST 请求类型

        if (this.isPost) {
            let rules = {
                username: {
                    string: true,       // 字段类型为 String 类型
                    required: true,     // 字段必填
                    trim: true,         // 字段需要trim处理
                    method: 'POST'       // 指定获取数据的方式
                },
                password: {
                    string: true,       // 字段类型为 String 类型
                    required: true,     // 字段必填
                    trim: true,         // 字段需要trim处理
                    method: 'POST'       // 指定获取数据的方式
                }

            };

            // 自定义 app_id 的错误信息
            let msgs = {
                username: '用户名不能为空',
                password: '密码不能为空'
            };

            if(!this.validate(rules, msgs)) {
                return this.fail(this.validateErrors);
            }

            // // this.rules=rules;
            // let flag = this.validate(rules);
            // if (!flag) {
            //     return this.fail('validate error', this.validateErrors);
            //     // 如果校验失败，返回
            //     // {"errno":1000,"errmsg":"validate error","data":{"username":"username can not be blank"}}
            // }
        }

    }
};
