module.exports = class extends think.Logic {
    addAction() {
        this.allowMethods = 'get,post'; // 允许 GET、POST 请求类型

        if (this.isPost) {
            let rules = {
                key: {
                    string: true,       // 字段类型为 String 类型
                    required: true,     // 字段必填
                    // default: 'thinkjs', // 字段默认值为 'thinkjs'
                    trim: true,         // 字段需要trim处理
                    method: 'POST'       // 指定获取数据的方式
                },
                value: {
                    string: true,       // 字段类型为 String 类型
                    required: true,     // 字段必填
                    // default: 'thinkjs', // 字段默认值为 'thinkjs'
                    trim: true,         // 字段需要trim处理
                    method: 'POST'       // 指定获取数据的方式
                },
                title: {
                    string: true,       // 字段类型为 String 类型
                    required: true,     // 字段必填
                    // default: 'thinkjs', // 字段默认值为 'thinkjs'
                    trim: true,         // 字段需要trim处理
                    method: 'POST'       // 指定获取数据的方式
                },
                sort: {
                    int: {min: 0, max: 100},// 0到100之间的整数
                    default: 0
                },

            };

            // this.rules=rules;
            let flag = this.validate(rules);
            if (!flag) {
                return this.fail('validate error', this.validateErrors);
                // 如果校验失败，返回
                // {"errno":1000,"errmsg":"validate error","data":{"username":"username can not be blank"}}
            }
        }

    }
};
