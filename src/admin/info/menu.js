const Types=think.Mongoose.mongoose.Types;
module.exports = async function () {
    /**
     * 支持2级菜单
     * 基本原则：
     * 按照模块分组菜单
     */
    //创建模块的主菜单id
    //const id = new Types.ObjectId();
    const Menu = think.mongoose('Menu');

    //删除可能已经存在的不一致的数据
    await Menu.remove({module: 'admin'}).exec();

    // await Menu.create({
    //     _id: id,
    //     parent: null,
    //     title: null,
    //     tip: '',
    //     rank: 0,
    //     hide: true,
    //     url: null,
    //     group: '系统',
    //     module: 'admin',
    //     icon: null
    // });

    await Menu.create({
        _id: new Types.ObjectId(),
       // parent: id,
        title: '配置管理',
        tip: '系统配置管理',
        rank: 0,
        hide: false,
        url: '/admin/config/index',
        group: '系统',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        // parent: id,
        title: '获取系统配置分页数据',
        tip: '',
        rank: 0,
        hide: true,
        url: '/admin/config/table',
        group: '系统',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        // parent: id,
        title: '删除配置项目',
        tip: '',
        rank: 0,
        hide: true,
        url: '/admin/config/delete',
        group: '系统',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        // parent: id,
        title: '启用配置项目',
        tip: '',
        rank: 0,
        hide: true,
        url: '/admin/config/enable',
        group: '系统',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        // parent: id,
        title: '禁止配置项目',
        tip: '',
        rank: 0,
        hide: true,
        url: '/admin/config/disable',
        group: '系统',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        // parent: id,
        title: '添加或者修改配置项目',
        tip: '',
        rank: 0,
        hide: true,
        url: '/admin/config/add',
        group: '系统',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
      //  parent: id,
        title: '模块管理',
        tip: '系统模块管理',
        rank: 1,
        hide: false,
        url: '/admin/module/index',
        group: '系统',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //  parent: id,
        title: '获取模块分页数据',
        tip: '',
        rank: 1,
        hide: true,
        url: '/admin/module/table',
        group: '系统',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //  parent: id,
        title: '重置模块数据',
        tip: '',
        rank: 1,
        hide: true,
        url: '/admin/module/reset',
        group: '系统',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //  parent: id,
        title: '安装模块',
        tip: '',
        rank: 1,
        hide: true,
        url: '/admin/module/install',
        group: '系统',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //  parent: id,
        title: '卸载模块',
        tip: '',
        rank: 1,
        hide: true,
        url: '/admin/module/uninstall',
        group: '系统',
        module: 'admin',
        icon: null
    });

    //
    // await Menu.create({
    //     _id: new Types.ObjectId(),
    //     parent: id,
    //     title: '插件管理',
    //     tip: '系统插件管理',
    //     rank: 2,
    //     hide: false,
    //     url: '/admin/addon/index',
    //     group: '系统',
    //     module: 'admin',
    //     icon: null
    // });


    await Menu.create({
        _id: new Types.ObjectId(),
      //  parent: id,
        title: '后台菜单管理',
        tip: '后台管理系统的导航菜单维护',
        rank: 5,
        hide: false,
        url: '/admin/menu/index',
        group: '系统',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //  parent: id,
        title: '获取菜单分页数据',
        tip: '',
        rank: 5,
        hide: true,
        url: '/admin/menu/table',
        group: '系统',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //  parent: id,
        title: '删除后台管理菜单数据',
        tip: '',
        rank: 5,
        hide: true,
        url: '/admin/menu/delete',
        group: '系统',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //  parent: id,
        title: '重值后台管理菜单数据',
        tip: '',
        rank: 5,
        hide: true,
        url: '/admin/menu/reset',
        group: '系统',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
      //  parent: id,
        title: '权限管理',
        tip: '后台访问权限定义',
        rank: 6,
        hide: false,
        url: '/admin/permission/index',
        group: '用户管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //  parent: id,
        title: '获取权限分页数据',
        tip: '',
        rank: 6,
        hide: true,
        url: '/admin/permission/table',
        group: '用户管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
     //   parent: id,
        title: '角色定义',
        tip: '系统角色定义',
        rank: 6,
        hide: false,
        url: '/admin/role/index',
        group: '用户管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '获取角色分页数据',
        tip: '',
        rank: 6,
        hide: true,
        url: '/admin/role/table',
        group: '用户管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '获取角色分页数据',
        tip: '',
        rank: 6,
        hide: true,
        url: '/admin/role/table',
        group: '用户管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '删除角色',
        tip: '',
        rank: 6,
        hide: true,
        url: '/admin/role/delete',
        group: '用户管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '设置角色访问菜单的权限',
        tip: '',
        rank: 6,
        hide: true,
        url: '/admin/role/menuInRole',
        group: '用户管理',
        module: 'admin',
        icon: null
    });


    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '管理员帐号管理',
        tip: '管理员帐号管理',
        rank: 6,
        hide: false,
        url: '/admin/administrator/index',
        group: '用户管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '获取管理员分页数据',
        tip: '',
        rank: 6,
        hide: true,
        url: '/admin/administrator/table',
        group: '用户管理',
        module: 'admin',
        icon: null
    });


    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '用户帐号管理',
        tip: '业务系统用户帐号管理',
        rank: 6,
        hide: false,
        url: '/admin/user/index',
        group: '用户管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '获取业务系统用户帐号分页数据',
        tip: '业务系统用户帐号管理',
        rank: 6,
        hide: true,
        url: '/admin/user/table',
        group: '用户管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //  parent: id,
        title: '图片选择控件',
        tip: '系统图片管理',
        rank: 4,
        hide: true,
        url: '/admin/picture/gallery',
        group: '内容管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '图片选择控件读取图片分页数据',
        tip: '',
        rank: 6,
        hide: true,
        url: '/admin/picture/galleryTable',
        group: '内容管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //  parent: id,
        title: '图片管理',
        tip: '系统图片管理',
        rank: 4,
        hide: false,
        url: '/admin/picture/index',
        group: '内容管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '读取图片分页数据',
        tip: '',
        rank: 6,
        hide: true,
        url: '/admin/picture/table',
        group: '内容管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '删除图片',
        tip: '',
        rank: 6,
        hide: true,
        url: '/admin/picture/delete',
        group: '内容管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '添加图片',
        tip: '',
        rank: 6,
        hide: true,
        url: '/admin/picture/add',
        group: '内容管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '文章管理',
        tip: '文章管理',
        rank: 6,
        hide: false,
        url: '/admin/article/index',
        group: '内容管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '读取文章分页数据',
        tip: '',
        rank: 6,
        hide: true,
        url: '/admin/article/table',
        group: '内容管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '评论管理',
        tip: '评论管理',
        rank: 6,
        hide: false,
        url: '/admin/comment/index',
        group: '内容管理',
        module: 'admin',
        icon: null
    });

    await Menu.create({
        _id: new Types.ObjectId(),
        //   parent: id,
        title: '读取评论分页数据',
        tip: '',
        rank: 6,
        hide: true,
        url: '/admin/comment/table',
        group: '内容管理',
        module: 'admin',
        icon: null
    });
};