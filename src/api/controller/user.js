const Base = require("./base");
const jwt = require('jsonwebtoken');

module.exports = class extends Base {
    // http://localhost:3000/api/user/detail?id=5aa37bbaed9b406ad1f45780
    async detailAction() {
        const id = this.query('id');
        let User = this.mongoose('User', {}, 'admin');

        const userId = this.ctx.state.user;//Auth操作在设置token的时候设置了 userId值
        if (userId !== id) {
            this.ctx.throw(401, '非法访问，只能获取已经登陆的用户自己的信息。');
        }

        //需要返回POJO对象
        let user = await User.findById(id).lean().exec();
        if (user) {
            //删除敏感信息
            const {username, email, mobile, _id} = user;
            this.success({username, email, mobile, id: _id});
        } else {
            this.ctx.throw(401, '非法访问，只能获取已经登陆的用户自己的信息。');
        }
    }
};
