// 数据格式
// const rules = {
//     "roles": {
//         "guest": {},
//         "reader": {
//             "permissions": ["read"],
//             "inherited": ["guest"]
//         },
//         "writer": {
//             "permissions": ["create"],
//             "inherited": ["reader"]
//         },
//         "editor": {
//             "permissions": ["update"],
//             "inherited": ["reader"],
//             "attributes": ["dailySchedule"]
//         },
//         "director": {
//             "permissions": ["delete"],
//             "inherited": ["reader", "editor"],
//         },
//         "admin": {
//             "permissions": ["manage"],
//             "inherited": ["director"],
//             "attributes": ["hasSuperPrivilege"]
//         }
//     },
//     "users": {
//         "john.smith": ["writer"],
//         "root": ["admin"]
//     }
// }

module.exports = async function () {

    //https://www.npmjs.com/package/rbac-a
    const roles = await think.mongoose('Role',{},'admin').find({}).exec();

    console.log(roles);

    const users = await think.mongoose('Administrator',{},'admin').find({}).exec();

    console.log(users);

    let rules = {roles: {}, users: {}};
    if (roles) {
        for (let role of roles) {
            rules.roles[role._id] = {
                inherited: [role.parent],
                permissions: role.permissions || [],
                // attributes: []
            };
        }
    }

    if (users) {
        for (let user of users) {
            rules.users[user._id] = user.roles || [];
        }
    }

    return rules;
};