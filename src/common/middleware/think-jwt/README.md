# API接口授权访问中间件
> api模块下的接口都必须授权才能访问，系统使用jwt:JSON Web Tokens检查访问权限。

错误代码

代码|说明
---|---
401|非法Token,需要重新登陆获取新的Token

客户端首先通过 [http://www.sitename.com/api/auth/login](http://www.sitename.com/api/auth/login)方法登陆获取Token,其他的API
的调用都必须设置请求头：
```
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiNWE3ZDQyOGQzZDFlMGRjNjZlNTk2ZTM5IiwiaWF0IjoxNTE4MTU4NDc3LCJleHAiOjE1MTg3NjMyNzd9.xBBBCEihuOK-_g-pdkz4S8j10zat8ekxqh6lD4tR1oo
```
> 'Bearer  '后面的部分为Token

完整的例子
```
GET /api/user/detail?id=5a7d428d3d1e0dc66e596e39 HTTP/1.1
Host: dev.zengqs.com:3000
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0
Accept: */*
Accept-Language: zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiNWE3ZDQyOGQzZDFlMGRjNjZlNTk2ZTM5IiwiaWF0IjoxNTE4MTU4NDc3LCJleHAiOjE1MTg3NjMyNzd9.xBBBCEihuOK-_g-pdkz4S8j10zat8ekxqh6lD4tR1oo
Connection: keep-alive
```



