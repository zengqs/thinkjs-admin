const path = require('path');
// const wechat = require('think-wechat')

const isDev = think.env === 'development';
module.exports = [
    // {
    //     handle: wechat,
    //     match: '/wechat',
    //     options: {
    //         //token: <微信开发者token>,
    //         //appid: <appID>,
    //         // encodingAESKey: <消息对称加密串>,
    //         //checkSignature: true // 可选，默认为true。由于微信公众平台接口调试工具在明文模式下不发送签名，所以如要使用该测试工具，请将其设置为false
    //     }
    // },
    {
        handle: 'meta',
        options: {
            logRequest: isDev,
            sendResponseTime: isDev
        }
    },
    {
        handle: 'resource',
        enable: isDev,
        options: {
            root: path.join(think.ROOT_PATH, 'www'),
            publicPath: /^\/(public|favicon\.ico|admin\/static)/
        }
    },
    {
        handle: 'trace',
        enable: !think.isCli,
        options: {
            debug: isDev
        }
    },
    {
        handle: 'payload',
        options: {}
    },
    {
        handle: 'router',
        options: {}
    },
    {
        handle: 'jwt',
        options: {
            secret: 'your secret key',
            passthrough: true,
            expiresIn: '7 days'
        }
    },
    'logic',
    'controller',
    {
        handle: 'framework',
        options: async () => {
            //https://www.npmjs.com/package/rbac-a
            const roles = await think.mongoose('Role', {}, 'admin').find({}).exec();
            const users = await think.mongoose('Administrator', {}, 'admin').find({}).exec();
            let rules = {roles: {}, users: {}};
            if (roles) {
                for (let role of roles) {
                    rules.roles[role._id] = {
                        inherited: [role.parent],
                        permissions: role.permissions || [],
                        // attributes: []
                    };
                }
            }
            if (users) {
                for (let user of users) {
                    rules.users[user._id] = user.roles || [];
                }
            }

            return {
                rbac: rules
            };
        }
    },

];
