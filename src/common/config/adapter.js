const fileCache = require('think-cache-file');
const nunjucks = require('think-view-nunjucks');
const dateFilter = require('nunjucks-date-filter');
const helper = require('think-helper');

const fileSession = require('think-session-file');
// const mysql = require('think-model-mysql');
const {Console, File, DateFile} = require('think-logger3');
const path = require('path');
const isDev = think.env === 'development';


/**
 * cache adapter config
 * @type {Object}
 */
exports.cache = {
    type: 'file',
    common: {
        timeout: 24 * 60 * 60 * 1000 // millisecond
    },
    file: {
        handle: fileCache,
        cachePath: path.join(think.ROOT_PATH, 'runtime/cache'), // absoulte path is necessarily required
        pathDepth: 1,
        gcInterval: 24 * 60 * 60 * 1000 // gc interval
    }
};

/**
 * model adapter config
 * @type {Object}
 */
// exports.model = {
//   type: 'mysql',
//   common: {
//     logConnect: isDev,
//     logSql: isDev,
//     logger: msg => think.logger.info(msg)
//   },
//   mysql: {
//     handle: mysql,
//     database: '',
//     prefix: 'think_',
//     encoding: 'utf8',
//     host: '127.0.0.1',
//     port: '',
//     user: 'root',
//     password: 'root',
//     dateStrings: true
//   }
// };

exports.model = {
    type: 'mongoose',
    mongoose: {
        host: '127.0.0.1',
        user: '',
        password: '',
        database: 'test',
        useCollectionPlural: false, //collection不使用复数形式
        options: {},
        // prefix: 'think_',
    },
};

// exports.model = {
//     type: 'mongoose',
//     mongoose: {
//         connectionString: 'mongodb://localhost/test',
//         options: {}
//     }
// };


/**
 * session adapter config
 * @type {Object}
 */
exports.session = {
    type: 'file',
    common: {
        cookie: {
            name: 'thinkjs'
            // keys: ['werwer', 'werwer'],
            // signed: true
        }
    },
    file: {
        handle: fileSession,
        sessionPath: path.join(think.ROOT_PATH, 'runtime/session')
    }
};

/**
 * view adapter config
 * @type {Object}
 */
exports.view = {
    type: 'nunjucks',
    common: {
        viewPath: path.join(think.ROOT_PATH, 'www'),
        sep: '/',
        extname: '.html'
    },
    nunjucks: {
        handle: nunjucks,
        beforeRender: (env, nunjucks, options) => {
            env.addFilter('date', dateFilter);
            env.addGlobal('isEmpty', helper.isEmpty);
        }, // 模板渲染预处理
        options: { // 模板引擎额外的配置参数

        }
    }
};


/**
 * logger adapter config
 * @type {Object}
 */
exports.logger = {
    type: isDev ? 'console' : 'dateFile',
    console: {
        handle: Console
    },
    file: {
        handle: File,
        backups: 10, // max chunk number
        absolute: true,
        maxLogSize: 50 * 1024, // 50M
        filename: path.join(think.ROOT_PATH, 'logs/app.log')
    },
    dateFile: {
        handle: DateFile,
        level: 'ALL',
        absolute: true,
        pattern: '-yyyy-MM-dd',
        alwaysIncludePattern: true,
        filename: path.join(think.ROOT_PATH, 'logs/app.log')
    }
};
