
Application created by [ThinkJS](http://www.thinkjs.org)
基于Thinkjs 3.2.7 的快速后台管理程序框架
数据库使用mongoose访问MongoDB。

## 更新说明：
- 修改了think-mongoose adapter，更新后以源码的形式发布，在项目中/src/common/adapter/model/mongoose.js
- 增加了think-framework middleware，实现RBAC权限管理和后台菜单访问的授权检测
- 后台管理模块实现基本的用户管理、图片资源管理、角色、权限管理、后台菜单管理、配置管理、文章管理
- 集成为新公众号开发的SDK
- 集成API的鉴权
- 支持RESTFUL接口

## Install dependencies

```
npm install
```

## Start server

```
npm start
```

## Deploy with pm2

Use pm2 to deploy app on production enviroment.

```
pm2 startOrReload pm2.json
```

## 功能模块

后台菜单管理
![](menu.png)